<?php
declare(strict_types=1);

namespace {

    use Grifix\Kit\Cqrs\Command\CommandBus;
    use Grifix\Kit\Cqrs\Command\Queue\ExecuteCommandConsumer;
    use Grifix\Kit\EventBus\EventBus;
    use Grifix\Kit\EventBus\Queue\ExecuteListenerConsumer;
    use Grifix\Kit\Queue\Definition;

    return [
        'command_bus' => new Definition(ExecuteCommandConsumer::class, CommandBus::QUEUE_NAME, 2),
        'event_bus' => new Definition(ExecuteListenerConsumer::class, EventBus::QUEUE_NAME, 2)
    ];
}
