<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace {
    
    return [
        'main' => [
            'pattern' => '/{locale:locale?}',
            'handler' => 'grifix.common.default'
        ],
        'root' => [
            'pattern' => '/{locale:locale?}/grifix/common',
            'children' => [
                'request' => [
                    'pattern' => '/request/{alias?}/{view?}',
                    'handler' => 'grifix.common.Request'
                ]
            ],
        ],
    ];
}
