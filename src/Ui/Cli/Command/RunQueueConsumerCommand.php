<?php
declare(strict_types=1);

namespace Grifix\Common\Ui\Cli\Command;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Queue\ConsumerFactory\ConsumerFactoryInterface;
use Grifix\Kit\Queue\QueueInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class QueueWorkerCommand
 * @package Grifix\Common\Ui\Cli\Command
 */
class RunQueueConsumerCommand extends AbstractCommand
{
    protected const ARG_QUEUE = 'queue';
    protected const ARG_CONSUMER = 'consumer';

    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var ConsumerFactoryInterface
     */
    protected $consumerFactory;

    protected function configure()
    {
        $this
            ->setName('common:run_queue_consumer')
            ->setDescription('Runs queue consumer.')
            ->addArgument(self::ARG_QUEUE, InputArgument::REQUIRED, 'Queue name')
            ->addArgument(self::ARG_CONSUMER, InputArgument::REQUIRED, 'Queue consumer class');
    }

    protected function init()
    {
        parent::init();
        $this->queue = $this->getShared(QueueInterface::class);
        $this->consumerFactory = $this->getShared(ConsumerFactoryInterface::class);
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->queue->consume(
            $this->consumerFactory->createConsumer(
                $input->getArgument(self::ARG_CONSUMER)
            ),
            $input->getArgument(self::ARG_QUEUE)
        );
    }
}
