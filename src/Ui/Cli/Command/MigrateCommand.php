<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Common\Ui\Cli\Command;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Migration\Event\MigrateDownEvent;
use Grifix\Kit\Migration\MigrationRunnerInterface;
use Grifix\Kit\Migration\Event\MigrateUpEvent;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MigrateCommand
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\AbstractCommand
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MigrateCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('common:migrate')
            ->setDescription('Runs migrations.')
            ->addArgument('action', InputArgument::REQUIRED, 'up or down')
            ->addArgument(
                'migration',
                InputArgument::OPTIONAL,
                'Name of the migration e.g. (grifix.common.M201712080659)'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrationRunner = $this->getShared(MigrationRunnerInterface::class);
        $eventBus = $this->getShared(EventBusInterface::class);

        $counter = 0;

        $eventBus->listen(MigrateUpEvent::class, function (MigrateUpEvent $event) use ($output, &$counter) {
            $output->writeln($event->getMigrationClass() . ' was executed.');
            $counter++;
        });

        $eventBus->listen(MigrateDownEvent::class, function (MigrateDownEvent $event) use ($output, &$counter) {
            $output->writeln($event->getMigrationClass() . ' was executed.');
            $counter++;
        });

        if (!in_array($input->getArgument('action'), ['up', 'down'])) {
            throw new \Exception('Invalid action argument "' . $input->getArgument('action') . '"!');
        }

        if ($input->getArgument('action') == 'up') {
            $migrationRunner->up($input->getArgument('migration'));
        } elseif ($input->getArgument('action') == 'down') {
            $migrationRunner->down($input->getArgument('migration'));
        }

        if (!$counter) {
            $output->writeln('Nothing to migrate.');
        } else {
            $output->writeln('All migrations was executed successfully.');
        }
    }
}
