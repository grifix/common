<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Ui\Cli\Command;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Kernel\KernelInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateMigrationCommand
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CreateMigrationCommand extends AbstractCommand
{
    protected $template = <<<'CODE'
<?php

declare(strict_types = 1);

namespace {namespace};

use Grifix\Kit\Migration\AbstractMigration;

/**
 * Class M201712080659
 */

class {className} extends AbstractMigration
{
    public function up(): void
    {
        //TODO create up sql
    }
    
    public function down(): void
    {
        //TODO create down sql
    }
}

CODE;

    protected function configure()
    {
        $this
            ->setName('common:migration')
            ->setDescription('Creates new migration.')
            ->addArgument('module', InputArgument::REQUIRED, 'module name e.g. grifix.acl');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $moduleName = $input->getArgument('module');
        $arr = explode('.',$moduleName);
        if(count($arr) != 2){
            throw new InvalidModuleNameException($moduleName);
        }
        $module = $this->getShared(KernelInterface::class)->getModule($arr[0], $arr[1]);
        $fs = $this->getShared(FilesystemHelper::class);
        if($fs->isDir($module->getAppDir())){
            $dir = $module->getAppDir().'src/Infrastructure/Migration';
        }else{
            $dir = $module->getVendorDir().'/src/Infrastructure/Migration';
        }
        if(!$fs->isDir($dir)){
            $fs->mkDir($dir);
        }
        $className = 'M'.date('YmdHis');
        $namespace = ucfirst($module->getVendor()).'\\'.ucfirst($module->getName()).'\\Infrastructure\\Migration';
        $path = $dir.'/'.$className.'.php';
        $fs->writeToFile(
            $dir.'/'.$className.'.php',
            str_replace(
                ['{className}','{namespace}'],
                [$className, $namespace],
                $this->template
            )
        );
        
        $output->writeln('Migration "'.$path.'" was created!');
        
    }
}