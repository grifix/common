<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Common\Ui\Cli\Command;

use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Grifix\Kit\Migration\MigrationRunnerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class MigrateCommand
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\AbstractCommand
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InstallCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('common:install')
            ->setDescription('Runs installation script.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getShared(MigrationRunnerInterface::class)->down();
        $this->getShared(MigrationRunnerInterface::class)->up();
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            'Warning! this command will remove all data from database! Type "yes" if you are sure?',
            false,
            '/^yes/i'
        );


        if ($helper->ask($input, $output, $question) || !$input->isInteractive()) {
            foreach ($this->getShared(KernelInterface::class)->getModules() as $module) {
                /**@var $module ModuleInterface */
                $module->install();
            }
        }
    }
}
