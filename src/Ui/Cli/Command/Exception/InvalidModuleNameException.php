<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Ui\Cli\Command;

/**
 * Class InvalidModuleNameException
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidModuleNameException extends \Exception
{
    
    protected $moduleName;
    
    /**
     * InvalidModuleNameException constructor.
     *
     * @param string $moduleName
     */
    public function __construct(string $moduleName)
    {
        $this->moduleName = $moduleName;
        $this->message = 'Invalid module name "'.$moduleName.'"!';
        parent::__construct();
    }
}