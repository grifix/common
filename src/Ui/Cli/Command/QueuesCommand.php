<?php
declare(strict_types=1);

namespace Grifix\Common\Ui\Cli\Command;

use Grifix\Kit\Alias;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Queue\Definition;
use Grifix\Kit\Queue\DefinitionsCollector\DefinitionsCollectorInterface;
use http\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class QueueWorkerCommand
 * @package Grifix\Common\Ui\Cli\Command
 */
class QueuesCommand extends AbstractCommand
{

    protected const ARGUMENT_ACTION = 'action';
    protected const ACTION_START = 'start';
    protected const ACTION_STOP = 'stop';
    protected const ACTION_RESTART = 'restart';
    protected const SCREEN_NAME = 'queues';

    /**
     * @var DefinitionsCollectorInterface
     */
    protected $definitionsCollector;

    protected function configure()
    {
        $this
            ->setName('common:queues')
            ->setDescription('Start queues')
            ->addArgument(
                self::ARGUMENT_ACTION,
                InputArgument::REQUIRED,
                self::ACTION_START . '|' . self::ACTION_STOP . '|' . self::ACTION_RESTART
            );
    }

    public function init()
    {
        parent::init();
        $this->definitionsCollector = $this->getShared(DefinitionsCollectorInterface::class);
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument(self::ARGUMENT_ACTION);
        switch ($action) {
            case self::ACTION_START:
                $this->start($output);
                break;

            case self::ACTION_STOP:
                $this->stop($output);
                break;

            case self::ACTION_RESTART:
                $this->stop($output);
                $this->start($output);
                break;

            default:
                throw new \InvalidArgumentException('Unknown action "' . $action . '"');
        }
    }

    /**
     * @param OutputInterface $output
     */
    protected function start(OutputInterface $output)
    {
        foreach ($this->definitionsCollector->collectDefinitions() as $definition) {
            for ($i = 0; $i < $definition->getQuantity(); $i++) {
                $screenName = $this->makeScreenName($definition->getConsumerClass(), $i);
                if (!$this->screenExists($screenName)) {
                    $output->writeln('Starting ' . $screenName);
                    exec('screen -dm -S "' . $screenName . '" php ' . $this->getShared(Alias::ROOT_DIR) . '/cli.php common:run_queue_consumer "' . $definition->getQueueName() . '" "' . $definition->getConsumerClass() . '"');
                }
            }
        }

        $output->writeln('All queues started!');
    }

    /**
     * @param string $screenName
     * @return bool
     */
    protected function screenExists(string $screenName): bool
    {
        return boolval(shell_exec('screen -list | grep "' . $screenName . '"'));
    }

    /**
     * @param string $class
     * @param int $number
     * @return string
     */
    protected function makeScreenName(string $class, int $number)
    {
        return str_replace('\\', '_', $class) . '_' . $number;
    }

    /**
     * @param OutputInterface $output
     */
    protected function stop(OutputInterface $output)
    {

        foreach ($this->definitionsCollector->collectDefinitions() as $definition) {
            for ($i = 0; $i < $definition->getQuantity(); $i++) {
                $screenName = $this->makeScreenName($definition->getConsumerClass(), $i);
                if ($this->screenExists($screenName)) {
                    $output->writeln('Stopping ' . $screenName);
                    exec('screen -X -S "' . $screenName . '" quit');
                }
            }
        }
        $output->writeln('All queues stopped!');
    }
}
