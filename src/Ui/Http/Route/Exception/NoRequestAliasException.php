<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Ui\Http\Route\Exception;

/**
 * Class NoRequestAliasException
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\Http\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NoRequestAliasException extends \Exception
{
   protected $message = 'No request alias';
}