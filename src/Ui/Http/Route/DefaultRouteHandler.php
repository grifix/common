<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Ui\Http\Route;


use Grifix\Kit\Route\Handler\AbstractRouteHandler;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\View\ViewFactoryInterface;

/**
 * Class DefaultRouteHandler
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\Http\Dispatcher
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DefaultRouteHandler extends AbstractRouteHandler
{
    /**
     * {@inheritdoc}
     */
    public function handle(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        return $response->withContent(
            $this->getShared(ViewFactoryInterface::class)->create('grifix.kit.{skin}.lyt.default')->render()
        );
    }
}