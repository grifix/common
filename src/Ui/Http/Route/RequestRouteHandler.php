<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Common\Ui\Http\Route;

use Grifix\Acl\Application\Contract\UserFinder\UserDto;
use Grifix\Acl\Application\Query\GetSignedInUserQuery;
use Grifix\Acl\Application\Query\HasUserAccessQuery;
use Grifix\Common\Ui\Http\Route\Exception\InvalidRequestException;
use Grifix\Common\Ui\Http\Route\Exception\InvalidRequestMethodException;
use Grifix\Common\Ui\Http\Route\Exception\NoRequestAliasException;
use Grifix\Common\Ui\Http\Route\Exception\NoRequestViewException;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Route\Handler\AbstractRouteHandler;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Ui\Request\RequestDispatcherInterface;
use Grifix\Kit\View\Renderer\RendererFactoryInterface;
use Grifix\Kit\View\Skin\SkinFactoryInterface;
use Grifix\Kit\View\Skin\SkinInterface;
use Grifix\Kit\View\ViewFactoryInterface;

/**
 * Class RequestRouteHandler
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\Http\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RequestRouteHandler extends AbstractRouteHandler
{
    /**
     * @param RouteInterface $route
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     * @throws InvalidRequestException
     */
    public function handle(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $this->checkMethod($request->getMethod());
        if ($route->getParam('alias') && $route->getParam('view')) {
            return $this->handleSingleRequest($route, $request, $response);
        } elseif ($request->getQueryParam('requests')) {
            return $this->handleMultipleRequests($route, $request, $response);
        }
        throw new InvalidRequestException();
    }

    /**
     * @param RouteInterface $route
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     * @throws NoRequestAliasException
     * @throws NoRequestViewException
     */
    protected function handleMultipleRequests(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $results = [];
        foreach ($request->getQueryParam('requests') as $r) {
            if (!isset($r['alias'])) {
                throw new NoRequestAliasException();
            }
            if (!isset($r['view'])) {
                throw new NoRequestViewException();
            }
            if (!isset($r['params'])) {
                $r['params'] = [];
            }
            $data = $this->getShared(RequestDispatcherInterface::class)->dispatch(
                $r['alias'],
                $r['params'],
                $request->getMethod()
            );
            $results[] = $this->getShared(ViewFactoryInterface::class)->create($r['view'])->render(
                array_merge($r['params'], $data)
            );
        }

        return $response->withHeader('content-type', 'application/json')->withContent(json_encode($results));
    }

    /**
     * @param string $method
     *
     * @return void
     * @throws InvalidRequestMethodException
     */
    protected function checkMethod(string $method)
    {
        if (!in_array($method, ['GET', 'POST'])) {
            throw new InvalidRequestMethodException($method);
        }
    }

    /**
     * @param RouteInterface $route
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     * @throws InvalidRequestMethodException
     */
    protected function handleSingleRequest(
        RouteInterface $route,
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $viewAlias = $route->getParam('view');
        if ($viewAlias == 'json') {
            $viewAlias = 'grifix.common.json.prt.default';
        }
        $parsedBody = $request->getParsedBody();
        if (!is_array($parsedBody)) {
            $parsedBody = [];
        }
        $params = array_merge($request->getQueryParams(), $parsedBody, $request->getUploadedFiles());

        $data = $this->getShared(RequestDispatcherInterface::class)->dispatch(
            $route->getParam('alias'),
            $params,
            $request->getMethod()
        );
        $view = $this->getShared(ViewFactoryInterface::class)->create($viewAlias);
        $renderer = $this->getShared(RendererFactoryInterface::class)->createRenderer(
            $this->detectSkin($viewAlias)->getContentType()
        );

        return $renderer->render($response, $view, array_merge($params, $data));
    }

    /**
     * @param string $aclResource
     * @return bool
     * @throws \Throwable
     */
    protected function hasAccess(string $aclResource): bool
    {
        /**@var $user UserDto*/
        $user = $this->executeQuery(
            new GetSignedInUserQuery($this->getShared(SessionInterface::class)->getId())
        );

        return $this->executeQuery(
            new HasUserAccessQuery(
                $user->getId(),
                $aclResource
            )
        );
    }

    /**
     * @param string $viewAlias
     *
     * @return SkinInterface
     */
    protected function detectSkin(string $viewAlias): SkinInterface
    {
        $skinName = explode('.', $viewAlias)[2];
        if ($skinName == '{skin}') {
            return $this->getShared(ViewFactoryInterface::class)->getSkin();
        }

        return $this->getShared(SkinFactoryInterface::class)->createSkin($skinName);
    }
}
