<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Application\Query;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Cqrs\QueryInterface;

/**
 * Class GetConfigQueryHandler
 *
 * @category Grifix
 * @package  Grifix\Common\Application\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GetConfigQueryHandler
{

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * GetConfigQueryHandler constructor.
     * @param ConfigInterface $config
     */
    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
    }


    /**
     * {@inheritdoc}
     */
    public function __invoke(GetConfigQuery $query)
    {
        return $this->config->get($query->getPath());
    }
}
