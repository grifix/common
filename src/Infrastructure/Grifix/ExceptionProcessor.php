<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Infrastructure\Grifix;

use Grifix\Common\Infrastructure\Acl\AclServiceInterface;
use Grifix\Common\Infrastructure\Grifix\Exception\RequestAccessDeniedException;
use Grifix\Kit\Exception\DomainException;
use Grifix\Kit\Exception\ExceptionProcessorInterface;
use Grifix\Kit\Exception\UserException;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\Module\ModuleClassInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Route\Exception\NoRouteMatchException;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Validation\Exception\ValidationException;

/**
 * Class ExceptionProcessor
 *
 * @category Grifix
 * @package  Grifix\Common\Infrastructure\Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ExceptionProcessor implements ModuleClassInterface, ExceptionProcessorInterface
{
    use ModuleClassTrait;
    
    /**
     * {@inheritdoc}
     */
    public function process(\Throwable $exception): \Throwable
    {
        
        if ($exception instanceof ValidationException) {
            return $this->prepareValidationException($exception);
        }
        if ($exception instanceof DomainException) {
            return $this->prepareDomainException($exception);
        }
        if ($exception instanceof RequestAccessDeniedException) {
            return $this->prepareAccessDeniedException($exception);
        }
        if ($exception instanceof NoRouteMatchException) {
            return $this->prepare404Exception($exception);
        }
        
        return $exception;
    }
    
    /**
     * @param DomainException $exception
     *
     * @return UserException
     */
    protected function prepareDomainException(DomainException $exception): UserException
    {
        return new UserException(
            $this->getShared(TranslatorInterface::class)->translate($exception->getTranslationKey()),
            400,
            $exception
        );
    }
    
    /**
     * @param ValidationException $exception
     *
     * @return ValidationException
     */
    protected function prepareValidationException(ValidationException $exception): ValidationException
    {
        return new ValidationException($exception->getErrors(), 400, $exception);
    }
    
    /**
     * @param \Throwable $exception
     *
     * @return UserException
     */
    protected function prepare404Exception(\Throwable $exception)
    {
        return new UserException($this->getShared(TranslatorInterface::class)->translate('grifix.kit.msg_404'), 404,
            $exception);
    }
    
    /**
     * @param \Throwable $exception
     *
     * @return UserException
     */
    protected function prepareAccessDeniedException(\Throwable $exception): UserException
    {
        $acl = $this->getShared(AclServiceInterface::class);
        $translator = $this->getShared(TranslatorInterface::class);
        if (!$acl->isSignedInUser($this->getShared(SessionInterface::class)->getId())) {
            return new UserException(
                $translator->translate('grifix.kit.msg_unauthorized'),
                401,
                $exception
            );
        }
        
        return new UserException($translator->translate('grifix.kit.msg_accessDenied'), 403, $exception);
    }
}
