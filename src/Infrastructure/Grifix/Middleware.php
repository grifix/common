<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Common\Infrastructure\Grifix;

use Grifix\Kit\Alias;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Http\CookieInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequest;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Intl\Locale\LocaleFactoryInterface;
use Grifix\Kit\Intl\Locale\LocaleInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\Module\ModuleClassInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Middleware\AbstractMiddleware;
use Grifix\Kit\Route\Exception\NoRouteMatchException;
use Grifix\Kit\Route\RouteCollectionFactoryInterface;
use Grifix\Kit\Route\RouteCollectionInterface;
use Grifix\Kit\Route\RouteInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\View\Skin\SkinFactory;
use Grifix\Kit\View\ViewFactoryInterface;

/**
 * Class FirstMiddleware
 *
 * @category Grifix
 * @package  Grifix\Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Middleware extends AbstractMiddleware implements ModuleClassInterface
{
    use ModuleClassTrait;

    const SKIN_VARIABLE_NAME = 'grifix_kit_view_default_skin';
    const LOCALE_VARIABLE_NAME = 'grifix_kit_intl_locale';

    /**
     * @var ServerRequest
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * {@inheritdoc}
     */
    protected function before(ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->request = $request;
        $this->response = $response;

        $this->defineSkin();
        $routeCollection = $this->getIoc()->get(RouteCollectionFactoryInterface::class)->createFromConfig();
        $currentRoute = $routeCollection->match($request);
        $this->setShared(RouteCollectionInterface::class, $routeCollection);
        $this->setShared(Alias::CURRENT_ROUTE, $currentRoute);
        $this->detectLocale($request, $currentRoute);
        if ($currentRoute) {
            return $currentRoute->handle($this->request, $this->response);
        }
        throw new NoRouteMatchException($this->request, $this->response);
    }

    /**
     * {@inheritdoc}
     */
    protected function after(ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->request = $request;
        $this->response = $response;

        return $this->response;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RouteInterface|null $route
     *
     * @return void
     */
    protected function detectLocale(ServerRequestInterface $request, RouteInterface $route = null)
    {
        $enabledLocales = $this->getShared(ConfigInterface::class)->get('grifix.kit.intl.config.enabledLocales');
        $localeCode = $enabledLocales[0];
        $possibleLocales = [
            \Locale::acceptFromHttp($request->getServerParam(ServerRequestInterface::HTTP_ACCEPT_LANGUAGE, '')),
            $request->getCookieParam(self::LOCALE_VARIABLE_NAME),
            $this->getShared(SessionInterface::class)->get(self::LOCALE_VARIABLE_NAME),
            ($route) ? $route->getParam('locale') : null,
            $request->getQueryParam('__locale'),
        ];
        foreach ($possibleLocales as $possibleLocale) {
            if (in_array($possibleLocale, $enabledLocales)) {
                $localeCode = $possibleLocale;
            }
        }
        $locale = $this->getShared(LocaleFactoryInterface::class)->createLocale($localeCode);
        $this->getShared(CookieInterface::class)->set(self::LOCALE_VARIABLE_NAME, $localeCode);
        $this->getShared(SessionInterface::class)->set(self::LOCALE_VARIABLE_NAME, $localeCode);
        $this->getShared(TranslatorInterface::class)->setCurrentLang($locale->getLang()->getCode());
        $this->setShared(LocaleInterface::class, $locale);
        \Locale::setDefault($localeCode);
    }

    /**
     *
     * @return void
     */
    protected function defineSkin()
    {
        $skinName = $this->getShared(ViewFactoryInterface::class)->getSkin()->getName();
        if ($this->getShared(SessionInterface::class)->get(self::SKIN_VARIABLE_NAME)) {
            $skinName = $this->getShared(SessionInterface::class)->get(self::SKIN_VARIABLE_NAME);
        }
        if ($this->request->getCookieParam(self::SKIN_VARIABLE_NAME)) {
            $skinName = $this->request->getCookieParam(self::SKIN_VARIABLE_NAME);
        }
        if ($this->request->getQueryParam('_skin')) {
            $skinName = $this->request->getQueryParam('_skin');
        }

        $skin = $this->getShared(SkinFactory::class)->createSkin($skinName);

        if ($skin->isPersistent()) {
            $this->response = $this->response->withCookie(self::SKIN_VARIABLE_NAME, $skin->getName());
            $this->getShared(SessionInterface::class)->set(self::SKIN_VARIABLE_NAME, $skin->getName());
        }
        $this->getShared(ViewFactoryInterface::class)->setSkin($skin);
        $this->setShared(Alias::CURRENT_SKIN, $skin);
    }

}