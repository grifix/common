<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Common\Infrastructure\Grifix;

use Grifix\Common\Infrastructure\Acl\AclServiceInterface;
use Grifix\Common\Infrastructure\Grifix\Exception\RequestAccessDeniedException;
use Grifix\Kit\Alias;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\Module\ModuleCommandInterface;
use Grifix\Kit\Kernel\Module\ModuleClassTrait;
use Grifix\Kit\Mailer\MailerFactoryInterface;
use Grifix\Kit\Mailer\MailerInterface;
use Grifix\Kit\Session\SessionInterface;
use Grifix\Kit\Ui\Request\Event\BeforeDispatchRequestEvent;
use Grifix\Kit\View\Asset\Event\CombineAssetEvent;

/**
 * Class AbstractFeatureContext
 *
 * @category Grifix
 * @package  Grifix\Common\Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Bootstrap implements ModuleCommandInterface
{
    use ModuleClassTrait;

    /**
     * @return void
     */
    public function run(): void
    {
        $this->combineAssets();
        $this->checkRequestAccess();
        $this->registerMailer();
    }

    /**
     * @internal
     *
     * Полчает наймпсейс из пути к css или js файла
     *
     * @param $path
     *
     * @return array
     */
    public function getNamespace($path): array
    {
        $info = pathinfo($path);
        $arr = explode(DIRECTORY_SEPARATOR, str_replace($this->getShared(Alias::ROOT_DIR), '', $info['dirname']));
        $vendor = $arr[2];
        $module = $arr[3];
        $arr = array_slice($arr, 6);
        $result = [
            $vendor,
            $module,
        ];
        foreach ($arr as $v) {
            $result[] = $v;
        }
        $result[] = str_replace('.', '_', $info['filename']);

        return $result;
    }

    protected function combineAssets()
    {
        $that = $this;
        $this->getShared(EventBusInterface::class)->listen(
            CombineAssetEvent::class,
            function (CombineAssetEvent $event) use ($that) {
                /**@var $this CombineAssetEvent */
                $info = pathinfo($event->getAssetPath());
                $content = $event->getAssetContent();
                if ($info['extension'] == 'css') {
                    $cssPrefix = '.' . implode('_', $that->getNamespace($event->getAssetPath()));
                    $content = str_replace('._', $cssPrefix, $content);
                }

                if ($info['extension'] == 'js') {
                    $widgetName = '"' . implode('_', $that->getNamespace($event->getAssetPath())) . '"';
                    $constructorName = '"' . implode('.', $that->getNamespace($event->getAssetPath())) . '"';
                    $content = str_replace('gfx.createWidgetName()', $widgetName, $content);
                    $content = str_replace('gfx.createConstructorName()', $constructorName, $content);
                }

                $content = preg_replace_callback("/php\(\/\*(.*)\*\/\)/", function ($matches) use ($that) {
                    $translate = function (
                        $key,
                        $vars = [],
                        $quantity = 1,
                        $case = LangInterface::CASE_NOMINATIVE,
                        $form = LangInterface::FORM_SINGULAR
                    ) use ($that) {
                        return $that->getShared(TranslatorInterface::class)->translate(
                            $key,
                            $vars,
                            $quantity,
                            $case,
                            $form
                        );
                    };

                    return eval("return json_encode(" . $matches[1] . ');');
                }, $content);

                //TODO check this case and try to remove this setter
                $event->setAssetContent($content);
            }
        );
    }

    protected function registerMailer()
    {
        $that = $this;
        $this->getIoc()->set(MailerInterface::class, function () use ($that) {
            return $that->getShared(MailerFactoryInterface::class)->createMailer();
        });
    }

    /**
     * Check acl access on every reques
     *
     * @return void
     */
    protected function checkRequestAccess()
    {
        $ioc = $this->getIoc();
        $this->getShared(EventBusInterface::class)->listen(
            BeforeDispatchRequestEvent::class,
            function (BeforeDispatchRequestEvent $event) use ($ioc) {
                $session = $ioc->get(SessionInterface::class);
                $acl = $ioc->get(AclServiceInterface::class);

                if (!$acl->hasAccessToRequest($session->getId(), $event->getRequestAlias())) {
                    throw new RequestAccessDeniedException($event->getRequestAlias());
                }
            }
        );
    }
}
