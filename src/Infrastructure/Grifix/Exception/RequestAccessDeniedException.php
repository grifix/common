<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Infrastructure\Grifix\Exception;

/**
 * Class RequestAccessDeniedException
 *
 * @category Grifix
 * @package  Grifix\Common\Ui\Http\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RequestAccessDeniedException extends \Exception
{
    protected $requestAlias;
    
    /**
     * RequestAccessDeniedException constructor.
     *
     * @param string $requestAlias
     */
    public function __construct(string $requestAlias)
    {
        $this->message = 'Access for request "'.$requestAlias.'" denied!"';
        $this->requestAlias = $requestAlias;
    }
}