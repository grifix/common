<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Infrastructure\Acl;

use Grifix\Common\Infrastructure\Acl\Adapter\AdapterInterface;
use Grifix\Common\Infrastructure\Acl\Exception\AclResourceIsNotDefinedException;
use Grifix\Kit\Config\ConfigInterface;

/**
 * Class AclService
 *
 * @category Grifix
 * @package  Grifix\Common\Infrastructure
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AclService implements AclServiceInterface
{
    
    /**
     * @var ConfigInterface
     */
    protected $config;
    
    /**
     * @var AdapterInterface
     */
    protected $adapter;
    
    /**
     * AclService constructor.
     *
     * @param AdapterInterface $adapter <Grifix\Common\Infrastructure\Acl\Adapter\GrifixAdapter>
     * @param ConfigInterface  $config
     */
    public function __construct(AdapterInterface $adapter, ConfigInterface $config)
    {
        $this->config = $config;
        $this->adapter = $adapter;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function hasAccessToRequest(string $sessionId, string $requestAlias)
    {
        $arr = explode('.', $requestAlias);
        assert(count($arr) > 3, 'Invalid request handler alias "' . $requestAlias . '"');
        $vendor = array_shift($arr);
        $module = array_shift($arr);
        
        $resource = $this->config->get($vendor . '.' . $module . '.acl.requests.' . implode('.', $arr));
        if (is_null($resource)) {
            throw new AclResourceIsNotDefinedException($requestAlias);
        }
        
        if (is_bool($resource)) {
            return $resource;
        }
        
        return $this->adapter->hasAccess($sessionId, $resource);
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSignedInUser(string $sessionId):bool
    {
        return !$this->adapter->getSignedInUser($sessionId)->getIsGuest();
    }
}
