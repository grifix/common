<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Infrastructure\Acl\Dto;

/**
 * Class UserModel
 *
 * @category Grifix
 * @package  Grifix\Common\Infrastructure\Acl\Dto
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserDto
{
    /**
     * @var int
     */
    protected $id;
    
    /**
     * @var string
     */
    protected $email;
    
    /**
     * @var bool
     */
    protected $isGuest;
    
    /**
     * UserDto constructor.
     *
     * @param string    $id
     * @param string $email
     * @param bool   $isGuest
     */
    public function __construct(string $id, string $email, bool $isGuest)
    {
        $this->id = $id;
        $this->email = $email;
        $this->isGuest = $isGuest;
    }
    
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    /**
     * @return bool
     */
    public function getIsGuest(): bool
    {
        return $this->isGuest;
    }
}
