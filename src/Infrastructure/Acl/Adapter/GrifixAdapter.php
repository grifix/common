<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Common\Infrastructure\Acl\Adapter;

use Grifix\Acl\Application\Query\GetSignedInUserQuery;
use Grifix\Acl\Application\Query\HasUserAccessQuery;
use Grifix\Common\Infrastructure\Acl\Dto\UserDto;
use Grifix\Kit\Cqrs\Query\QueryBusInterface;
use Grifix\Acl\Application\Contract\UserFinder\UserDto as AclUserDto;

/**
 * Class GrifixAdapter
 *
 * @category Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GrifixAdapter implements AdapterInterface
{
    /**
     * @var QueryBusInterface
     */
    protected $queryBus;
    
    /**
     * GrifixAdapter constructor.
     *
     * @param QueryBusInterface $queryBus
     */
    public function __construct(QueryBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasAccess(string $sessionId, string $resource): bool
    {
        /**@var $user AclUserDto */
        $user = $this->queryBus->execute(new GetSignedInUserQuery($sessionId));

        return $this->queryBus->execute(new HasUserAccessQuery($user->getId(), $resource));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSignedInUser(string $sessionId):UserDto
    {
        /**@var $userModel UserDto */
        $userModel = $this->queryBus->execute(new GetSignedInUserQuery($sessionId));
        
        return new UserDto($userModel->getId(), $userModel->getEmail(), $userModel->getIsGuest());
    }
}
