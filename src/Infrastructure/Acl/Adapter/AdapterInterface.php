<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Common\Infrastructure\Acl\Adapter;

use Grifix\Common\Infrastructure\Acl\Dto\UserDto;

/**
 * Class GrifixAdapter
 *
 * @category Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface AdapterInterface
{
    /**
     * @param string $sessionId
     * @param string $resource
     *
     * @return bool
     */
    public function hasAccess(string $sessionId, string $resource): bool;
    
    /**
     * @param string $sessionId
     *
     * @return UserDto
     */
    public function getSignedInUser(string $sessionId): UserDto;
}